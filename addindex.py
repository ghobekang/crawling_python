
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from timeloop import Timeloop

cred = credentials.Certificate('osori-4515b-765033b25b28.json')
firebase_admin.initialize_app(cred)

db = firestore.client()
DOC_REF = db.collection(u'contents').document(u'commons').collection('economics')

lists = []

docs = DOC_REF.get()
for (i, doc) in enumerate(docs) :
    lists.append(doc.to_dict())

with open('economics.txt', 'w') as f:
    for item in lists:
        f.write("%s\n" % item)