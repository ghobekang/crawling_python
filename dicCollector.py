import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from timeloop import Timeloop

cred = credentials.Certificate('osori-4515b-765033b25b28.json')
firebase_admin.initialize_app(cred)

db = firestore.client()
tl = Timeloop()
TIMELOOP_INTERVAL = 24*60*60

def delete_collection(coll_ref, batch_size):
    docs = coll_ref.limit(10).get()
    deleted = 0

    for doc in docs:
        print(u'Deleting doc {} => {}'.format(doc.id, doc.to_dict()))
        doc.reference.delete()
        deleted = deleted + 1

    if deleted >= batch_size:
        return delete_collection(coll_ref, batch_size)

@tl.job(interval=timedelta(seconds=TIMELOOP_INTERVAL))
def intervalFunctionDaily() :
    url = "http://www.ddaily.co.kr/news/article_list_all.html"
    soup = getDocument(url)
    crawler(soup)

def getDocument(url) :
    if url.startswith('/') :
        url = "http://www.ddaily.co.kr" + url 
    response = requests.get(url)
    soup = BeautifulSoup(response.content, "lxml", from_encoding="utf-8")
    return soup

def crawler(soup) :
    datelists = soup.find('div',{'class': 'm01_ara'}).findAll('span')
    count = 0
    today = datetime.now()
    date_ofToday = str(today.isoweekday())

    doc_ref = db.collection(u'articles').document(u'weekly')
    if doc_ref.collection(date_ofToday).get() != 'none' :
        delete_collection(doc_ref.collection(date_ofToday), 10)

    for date in datelists :
        dateItem = date.get_text().split(' ')[1].split('.')
        
        articleDate = datetime(int(dateItem[0]),int(dateItem[1]),int(dateItem[2]))
        ONE_DAY_TIME = 60*60*24

        if (today - articleDate).seconds < ONE_DAY_TIME :
            title = date.parent.parent.find('dt').get_text()
            try:
                thumbnailImg = "http://www.ddaily.co.kr" + date.parent.parent.find('img')['src']    
            except TypeError:
                pass
            detail_link = date.parent.parent.find('a')['href']
            detail = getDocument(detail_link).find('div',{'id': 'news_body_area'})
            detailDoc = detail.get_text()
            try:
                detailImgLink = detail.find('img')['src']
            except TypeError:
                pass

            doc_ref.collection(date_ofToday).add({
                'title' : title,
                'thumbnailLink' : thumbnailImg,
                'content' : detailDoc,
                'contentImgLink' : detailImgLink
            })
            count += 1
            print(count)
  

if __name__ == "__main__":
    intervalFunctionDaily()
    tl.start(block=True)
    